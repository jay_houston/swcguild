
function rollDice(count){
    var die1 = document.getElementById("die1");
    var die2 = document.getElementById("die2");
    var status = document.getElementById("status");
    var d1 = Math.floor(Math.random() * 6) + 1;
    var d2 = Math.floor(Math.random() * 6) + 1;
    var diceTotal = d1 + d2;
    die1.innerHTML = d1;
    die2.innerHTML = d2;
    status.innerHTML = "You rolled "+diceTotal+".";
    if(diceTotal == "7"){
        status.innerHTML += " You won 4 dollars!!";
        winLoss = true;
        Stats(count, winLoss);
    }
    else {
        status.innerHTML += " You lost 1 dollar.";
        winLoss = false;
        Stats(count, winLoss);
    }
    
    
}
var count = 0;
function Count() {
    count += 1;
    document.getElementById("clicks").innerHTML = count;
    rollDice(count);
}
var total = 0;
var percent = 0;
gamesWon = 0;
gamesLost = 0;

function Stats(count, winLoss) {

    if (winLoss === true) {
        total += 4;
        document.getElementById("totalWon").innerHTML = total;
        gamesWon += 1;
        Percentage(count, gamesWon);
    }
    else if(winLoss === false) {
        total += -1;
        document.getElementById("totalWon").innerHTML = total;
        Percentage(count, gamesWon);
    }
}

function Percentage(count, gamesWon) {
    
    percent = (gamesWon / count * 100).toFixed(2);
    document.getElementById("wins").innerHTML = gamesWon;
    document.getElementById("percentWon").innerHTML = percent;
}
function Reset() {
    count = 0;
    total = 0;
    percent = 0;
    gamesWon = 0;
    d1 = 0;
    d2 = 0;
    document.getElementById("wins").innerHTML = gamesWon;
    document.getElementById("percentWon").innerHTML = percent;
    document.getElementById("totalWon").innerHTML = total;
    document.getElementById("clicks").innerHTML = count;
    document.getElementById("die1").innerHTML=d1;
    document.getElementById("die2").innerHTML=d2;
    document.getElementById("status").innerHTML=null;
}


    